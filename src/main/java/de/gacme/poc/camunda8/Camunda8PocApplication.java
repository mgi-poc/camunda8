package de.gacme.poc.camunda8;

import io.camunda.zeebe.spring.client.annotation.Deployment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Deployment(resources = {
        "classpath:mgv-merge-client.bpmn",
        "classpath:verify-domain-finished-client-merge.form"
})
public class Camunda8PocApplication {

    public static void main(String[] args) {
        SpringApplication.run(Camunda8PocApplication.class, args);
    }

}
