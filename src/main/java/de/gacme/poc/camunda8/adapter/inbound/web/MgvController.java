package de.gacme.poc.camunda8.adapter.inbound.web;

import de.gacme.poc.camunda8.adapter.inbound.web.model.DomainMergeFinished;
import de.gacme.poc.camunda8.adapter.inbound.web.model.MGV;
import io.camunda.zeebe.client.ZeebeClient;
import io.camunda.zeebe.client.api.response.ProcessInstanceEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/mgv/merge")
@RequiredArgsConstructor
@Slf4j
public class MgvController {

    public static final String MGV_MERGE_CLIENT_PROCESS_ID = "MgvMergeClient";
    private final ZeebeClient zeebeClient;

    @PostMapping
    public UUID startProcess(@RequestBody MGV mgv) {

        UUID clientMergeId = UUID.randomUUID();

        ProcessInstanceEvent event = zeebeClient.newCreateInstanceCommand()
                .bpmnProcessId(MGV_MERGE_CLIENT_PROCESS_ID)
                .latestVersion()
                .variables(Map.of(
                        "clientMergeId", clientMergeId.toString(),
                        "mvgCase", mgv.getMvgCase(),
                        "sourceClientUuid", mgv.getSourceClientUuid().toString(),
                        "targetClientUuid", mgv.getTargetClientUuid().toString()))
                .send()
                .join();

        log.info("started a merge {} with clientMergeId {}", mgv, clientMergeId);

        return clientMergeId;
    }

    @PutMapping("{client-merge-id}")
    public void startProcess(@PathVariable("client-merge-id") UUID clientMergeId, @RequestBody DomainMergeFinished domainMergeFinished) {

        zeebeClient.newPublishMessageCommand()
                .messageName(domainMergeFinished.getDomain().getDomainFinishedMessageName())
                .correlationKey(clientMergeId.toString())
                .timeToLive(Duration.ofMinutes(1))
                .tenantId(zeebeClient.getConfiguration().getDefaultTenantId())
                .send()
                .join();

        log.info("domain {} finished a merge: {}", domainMergeFinished.getDomain(), clientMergeId);
    }
}
