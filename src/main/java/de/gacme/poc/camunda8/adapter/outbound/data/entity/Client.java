package de.gacme.poc.camunda8.adapter.outbound.data.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
public class Client {
    private UUID uuid;
    private UUID parent;
    private String name;
    private boolean locked;
}
