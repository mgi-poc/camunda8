package de.gacme.poc.camunda8.adapter.inbound.web.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class MGV {

    private String mvgCase;

    private UUID sourceClientUuid;

    private UUID targetClientUuid;
}
