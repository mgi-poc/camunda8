package de.gacme.poc.camunda8.adapter.camunda.worker;

import de.gacme.poc.camunda8.adapter.outbound.data.ClientRepository;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import io.camunda.zeebe.spring.client.annotation.Variable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
public class RetreiveMergeInformationJobWorker {

  private final ClientRepository clientRepository;

  @JobWorker(type = "retreive-merge-client-information")
  public Map<String, Object> retreiveMergeInformation(@Variable(name = "sourceClientUuid") UUID sourceClientUuid,
                                       @Variable(name = "targetClientUuid") UUID targetClientUuid) {
    return Map.of(
            "sourceClientExists", clientRepository.findByPK(sourceClientUuid).isPresent(),
            "targetClientExists", clientRepository.findByPK(targetClientUuid).isPresent()
    );
  }

}