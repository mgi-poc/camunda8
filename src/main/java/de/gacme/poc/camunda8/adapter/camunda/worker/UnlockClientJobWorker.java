package de.gacme.poc.camunda8.adapter.camunda.worker;

import de.gacme.poc.camunda8.adapter.outbound.data.ClientRepository;
import de.gacme.poc.camunda8.adapter.outbound.data.entity.Client;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import io.camunda.zeebe.spring.client.annotation.Variable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
public class UnlockClientJobWorker {

  private final ClientRepository clientRepository;

  @JobWorker(type = "unlock-client")
  public void unlockClient(@Variable(name = "clientUuid") UUID clientUuid) {
    log.info("unlock client {}", clientUuid);
    Client client = clientRepository.findByPK(clientUuid)
            .orElseThrow();
    client.setLocked(false);
  }

}