package de.gacme.poc.camunda8.adapter.camunda.worker;

import de.gacme.poc.camunda8.adapter.outbound.data.ClientRepository;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import io.camunda.zeebe.spring.client.annotation.Variable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
public class DeleteClientJobWorker {

  private final ClientRepository clientRepository;

  @JobWorker(type = "delete-client")
  public void deleteClient(@Variable(name = "clientUuid") UUID clientUuid) {
    log.info("delete client {}", clientUuid);
    clientRepository.delete(clientUuid);
  }

}