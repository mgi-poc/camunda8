package de.gacme.poc.camunda8.adapter.inbound.web.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Domain {
    DOMAIN_A("DomainAMergeFinishedMessage"),
    DOMAIN_B("DomainBMergeFinishedMessage");

    private final String domainFinishedMessageName;
}
