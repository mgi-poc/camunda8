package de.gacme.poc.camunda8.adapter.inbound.web.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DomainMergeFinished {

    private Domain domain;
}
