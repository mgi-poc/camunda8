package de.gacme.poc.camunda8.adapter.outbound.data;


import de.gacme.poc.camunda8.adapter.outbound.data.entity.Client;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Component
public class ClientRepository {

    private static final Map<UUID, Client> CLIENTS = new HashMap<>();

    {
        upsert(Client.builder()
                .uuid(UUID.fromString("00000000-0000-0000-1001-000000000001"))
                .parent(UUID.fromString("00000000-0000-0000-1001-000000000000"))
                .name("Alice").locked(false).build());
        upsert(Client.builder()
                .uuid(UUID.fromString("00000000-0000-0000-1001-000000000002"))
                .parent(UUID.fromString("00000000-0000-0000-1001-000000000000"))
                .name("Bob").locked(false).build());
        upsert(Client.builder()
                .uuid(UUID.fromString("00000000-0000-0000-1002-000000000001"))
                .parent(UUID.fromString("00000000-0000-0000-1002-000000000000"))
                .name("Alice").locked(false).build());
        upsert(Client.builder()
                .uuid(UUID.fromString("00000000-0000-0000-1002-000000000002"))
                .parent(UUID.fromString("00000000-0000-0000-1002-000000000000"))
                .name("Marshal")
                .locked(false)
                .build());
    }

    public void upsert(Client client) {
        CLIENTS.put(client.getUuid(), client);
    }

    public Optional<Client> findByPK(UUID pk) {
        return Optional.ofNullable(CLIENTS.get(pk));
    }

    public void delete(UUID pk) {
        CLIENTS.remove(pk);
    }
}
